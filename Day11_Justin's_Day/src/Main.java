
public class Main 
{
	public static void main(String args[])
	{
		try
		{	
			wakeUp();
		
			getReadyForSchool();
		
			goToClass();
		
			eatLunch();
		
			goToMoreClass();
		
			goHome();
		}
		
		catch (AbductionException e)
		{
			System.out.println("Justin heard about an abduction on campus. Maybe it was an alien.");
			System.out.println("Justin goes back to sleep.");
		}
	}
	
	private static void wakeUp() 
	{
		System.out.println("Justin wakes up.");
		
		//Here's how to throw an exception
		
		//Option 1: Just ignore the exception and let the program terminate
		//throw new AbductionException();
		
		//Option 2: Catch the exception
		/*
		(try
		{
			throw new AbductionException();
		}
		
		catch (AbductionException e)
		{
			System.out.println("Justin's teacher was abducted by an alien. Hurray!");
			System.out.println("Justin goes back to sleep");
		}
		*/
		
		//Option 3: keep throwing exception
		throw new AbductionException();
	}
	
	private static void getReadyForSchool()
	{
		System.out.println("Justin gets ready for school.");
	}
	
	private static void goToClass()
	{
		System.out.println("Justin can't hear anything because of the fan");
	}
	
	private static void eatLunch()
	{
		System.out.println("Justin eats lunch. He has a... McRib 'sandwich");
	}
	
	private static void goToMoreClass()
	{
		System.out.println("Justin tries to stay awake in his later classes");
	}
	
	private static void goHome()
	{
		System.out.println("Justin takes his hoverboard home");
	}
}
